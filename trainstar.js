class Brain {
    static inputLayerSize = 3;
    static hiddenLayerSize = 5;
    static outputLayerSize = 3;
    static numHiddenLayers = 3;

    constructor(inputWeights, inputBiases, weights, biases, outputWeights, outputBiases) {
        this.inputWeights = inputWeights;
        this.inputBiases = inputBiases;
        this.weights = weights;
        this.biases = biases;
        this.outputWeights = outputWeights;
        this.outputBiases = outputBiases;
    }

    static random() {
        // Generate a 2d array of $inputLayerSize x $hiddenLayerSize
        const inputWeights = math.random([Brain.inputLayerSize, Brain.hiddenLayerSize], -1, 1)
        const inputBiases = math.random([Brain.hiddenLayerSize], -1, 1)
    
        // Generate a 3d array: a 2d array of $hiddenLayerSize x $hiddenLayerSize for every layer
        const weights = math.random([Brain.numHiddenLayers, Brain.hiddenLayerSize, Brain.hiddenLayerSize], -1, 1);
        const biases = math.random([Brain.numHiddenLayers, Brain.hiddenLayerSize], -1, 1);

        // Output layer reduces the size to the number of desired outputs
        const outputWeights = math.random([Brain.hiddenLayerSize, Brain.outputLayerSize], -1, 1);
        const outputBiases = math.random([Brain.outputLayerSize], -1, 1);

        return new Brain(inputWeights, inputBiases, weights, biases, outputWeights, outputBiases);
    }

    /**
    * sensors: array of $inputLayerSize floats
    *
    * returns: array of $outputLayerSize floats >= 0
    */
    decide(sensors) {
        sensors = math.matrix(sensors);

        // Apply input layer
        let result = math.multiply(sensors, this.inputWeights);
        result = math.add(result, this.inputBiases);

        // no activation layer after the input layer for now
        // result = math.map(result, (value) => value >= 0 ? value : 0 ); // ReLU

        // Apply hidden layers
        for (let i = 0; i < Brain.numHiddenLayers; i++) {
            result = math.multiply(result, this.weights[i]);
            result = math.add(result, this.biases[i]);

            result = math.map(result, (value) => value >= 0 ? value : 0 ); // ReLU
        }

        // Apply output layer
        result = math.multiply(sensors, this.inputWeights);
        result = math.add(result, this.inputBiases);

        // The final output will be keypresses, so we apply a binary activation for the output layer
        // result = math.map(result, (value) => value >= 0 ? value : 0 ); // ReLU
        result = math.map(result, (value) => value >= 0 ? 1 : 0 ) ; // binary

        return result.toArray();
    }

    createMutation() {
        const probability = 0.2;
        const maxdelta = 0.5;
        const maxbiasdelta = 0.1;

        function deltaMatrix(shape, max) {
            const mask = math.map(math.random(shape), (value) => value < probability ? 1 : 0 );
            let deltas = math.random(shape, max * -1, max);
            deltas = math.dotMultiply(deltas, mask);
            return deltas;
        }

        const num = Brain.numHiddenLayers;
        const size = Brain.hiddenLayerSize;
        let hiddenWeightDeltas = deltaMatrix([num, size, size], maxdelta)
        let hiddenBiasDeltas = deltaMatrix([num, size], maxbiasdelta)

        let inputWeightDeltas = deltaMatrix([Brain.inputLayerSize, Brain.hiddenLayerSize], maxdelta)
        let inputBiasDeltas = deltaMatrix([Brain.hiddenLayerSize], maxbiasdelta)

        let outputWeightDeltas = deltaMatrix([Brain.hiddenLayerSize, Brain.outputLayerSize], maxdelta)
        let outputBiasDeltas = deltaMatrix([Brain.outputLayerSize], maxbiasdelta)

        const inputWeights = math.add(this.inputWeights, inputWeightDeltas);
        const inputBiases = math.add(this.inputBiases, inputBiasDeltas);

        const hiddenWeights = math.add(this.weights, hiddenWeightDeltas);
        const hiddenBiases = math.add(this.biases, hiddenBiasDeltas);

        const outputWeights = math.add(this.outputWeights, outputWeightDeltas);
        const outputBiases = math.add(this.outputBiases, outputBiasDeltas);

        return new Brain(inputWeights, inputBiases, hiddenWeights, hiddenBiases, outputWeights, outputBiases);
    }
}

function newAgent(spr, brain) {
    let dinges = {
        matterSprite: spr,
        blocked: {
            left: false,
            right: false,
            bottom: false
        },
        numTouching: {
            left: 0,
            right: 0,
            bottom: 0
        },
        sensors: {
            bottom: null,
            left: null,
            right: null
        },
        time: {
            leftDown: 0,
            rightDown: 0
        },
        lastJumpedAt: 0,
        speed: {
            run: 3,
            jump: 5
        },
        brain: brain,
    };
    spr.parentdinges = dinges;
    dinges.smoothedControl = new SmoothedHorizontalControl(0.0005);

    return dinges;
}

// Smoothed horizontal controls helper. This gives us a value between -1 and 1 depending on how long
// the player has been pressing left or right, respectively.
class SmoothedHorizontalControl {
    constructor(speed) {
        this.msSpeed = speed;
        this.value = 0;
    }

    moveLeft(delta) {
        if (this.value > 0) { this.reset(); }
        this.value -= this.msSpeed * delta;
        if (this.value < -1) { this.value = -1; }
    }

    moveRight(delta) {
        if (this.value < 0) { this.reset(); }
        this.value += this.msSpeed * delta;
        if (this.value > 1) { this.value = 1; }
    }

    reset() {
        this.value = 0;
    }
}

class Example extends Phaser.Scene
{
    playerController;
    agents;
    cursors;
    cam;
    // smoothedControls; // array of SmoothedHorizontalControls, with the 0th being the player's and the remainder being the agents'
    downWasPressed;

    preload ()
    {
        this.load.tilemapTiledJSON('map', 'assets2/matter-platformer.json');
        this.load.image('kenney_redux_64x64', 'assets2/kenney_redux_64x64.png');
        this.load.spritesheet('player', 'assets2/dude-cropped.png', { frameWidth: 32, frameHeight: 42 });
        this.load.image('box', 'assets2/box-item-boxed.png');
        this.load.image('star', 'assets2/star.png');
    }

    create ()
    {
        const map = this.make.tilemap({ key: 'map' });
        const tileset = map.addTilesetImage('kenney_redux_64x64');
        const layer = map.createLayer(0, tileset, 0, 0);

        const initialAmount = 5;

        const spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

        this.downWasPressed = false;

        // Set up the layer to have matter bodies. Any colliding tiles will be given a Matter body.
        map.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(layer);

        this.matter.world.setBounds(map.widthInPixels, map.heightInPixels);
        this.matter.world.createDebugGraphic();
        this.matter.world.drawDebug = false;

        this.cursors = this.input.keyboard.createCursorKeys();
        // this.smoothedControls = [new SmoothedHorizontalControl(0.0005)];

        // The player is a collection of bodies and sensors
        this.playerController = {
            matterSprite: this.matter.add.sprite(0, 0, 'player', 4),
            blocked: {
                left: false,
                right: false,
                bottom: false
            },
            numTouching: {
                left: 0,
                right: 0,
                bottom: 0
            },
            sensors: {
                bottom: null,
                left: null,
                right: null
            },
            time: {
                leftDown: 0,
                rightDown: 0
            },
            lastJumpedAt: 0,
            speed: {
                run: 7,
                jump: 10
            }
        };
        this.playerController.smoothedControl = new SmoothedHorizontalControl(0.0005);

        this.agents = [];
        for (let i = 0; i < initialAmount; i++) {
            let agent = newAgent(this.matter.add.sprite(0, 0, 'player', 4), Brain.random());

            this.agents.push(agent);
        }

        const M = Phaser.Physics.Matter.Matter;
        const w = this.playerController.matterSprite.width;
        const h = this.playerController.matterSprite.height;

        // The player's body is going to be a compound body:
        //  - body is the solid body that will physically interact with the world. It has a
        //    chamfer (rounded edges) to avoid the problem of ghost vertices: http://www.iforce2d.net/b2dtut/ghost-vertices
        //  - Left/right/bottom sensors that will not interact physically but will allow us to check if
        //    the player is standing on solid ground or pushed up against a solid object.

        // Move the sensor to player center
        const sx = w / 2;
        const sy = h / 2;

        // The player's body is going to be a compound body.
        let agentsandplayer = this.agents.concat(this.playerController);
        for (let i in agentsandplayer) {
            const body = M.Bodies.rectangle(sx, sy, w * 0.75, h, { chamfer: { radius: 10 } });
            agentsandplayer[i].sensors.bottom = M.Bodies.rectangle(sx, h, sx, 5, { isSensor: true });
            agentsandplayer[i].sensors.left = M.Bodies.rectangle(sx - w * 0.45, sy, 5, h * 0.25, { isSensor: true });
            agentsandplayer[i].sensors.right = M.Bodies.rectangle(sx + w * 0.45, sy, 5, h * 0.25, { isSensor: true });
            
            const compoundBody = M.Body.create({
                parts: [
                    body,
                    agentsandplayer[i].sensors.bottom,
                    agentsandplayer[i].sensors.left,
                    agentsandplayer[i].sensors.right
                ],
                friction: 0.01,
                restitution: 0.05 // Prevent body from sticking against a wall
            });
            
            agentsandplayer[i].matterSprite
                .setExistingBody(compoundBody)
                .setFixedRotation() // Sets max inertia to prevent rotation
                .setPosition(600 + (i * 8), 1000);
        }

        this.matter.add.image(630, 750, 'box');
        this.matter.add.image(630, 650, 'box');
        this.matter.add.image(630, 550, 'box');

        this.cam = this.cameras.main;
        this.cam.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
        this.smoothMoveCameraTowards(this.playerController.matterSprite);

        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('player', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('player', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'idle',
            frames: this.anims.generateFrameNumbers('player', { start: 4, end: 4 }),
            frameRate: 10,
            repeat: -1
        });

        // Use matter events to detect whether the player is touching a surface to the left, right or
        // bottom.

        // Before matter's update, reset the player's count of what surfaces it is touching.
        this.matter.world.on('beforeupdate', function (event) {
            let agentsandplayer = this.agents.concat(this.playerController);
            for (let i in agentsandplayer) {
                agentsandplayer[i].numTouching.left = 0;
                agentsandplayer[i].numTouching.right = 0;
                agentsandplayer[i].numTouching.bottom = 0;
            }
        }, this);

        // Loop over the active colliding pairs and count the surfaces the player is touching.
        this.matter.world.on('collisionactive', function (event) {
            let agentsandplayer = this.agents.concat(this.playerController);
            for (let j in agentsandplayer) {
                const playerBody = agentsandplayer[j].body;
                const left = agentsandplayer[j].sensors.left;
                const right = agentsandplayer[j].sensors.right;
                const bottom = agentsandplayer[j].sensors.bottom;

                for (let i = 0; i < event.pairs.length; i++)
                {
                    const bodyA = event.pairs[i].bodyA;
                    const bodyB = event.pairs[i].bodyB;

                    // If we got a collision between a star and an agent that is not the player
                    if (bodyA.gameObject && bodyB.gameObject && bodyA.gameObject.texture && bodyB.gameObject.texture) {

                        if ((bodyA.gameObject.texture.key == 'player' && bodyB.gameObject.texture.key == 'player')) {
                            if (spacebar.isDown) {
                                let removeMe = null;
                                if (bodyA.gameObject == this.playerController.matterSprite) {
                                    removeMe = bodyB;
                                }
                                if (bodyB.gameObject == this.playerController.matterSprite) {
                                    removeMe = bodyA;
                                }
                                if (removeMe != null && removeMe.gameObject.body) {
                                    let idx = this.agents.findIndex(e => e.matterSprite.body.id === removeMe.gameObject.body.id);
                                    if (idx > -1) {
                                        this.agents[idx].matterSprite.destroy();
                                        this.agents.splice(idx, 1);
                                    }
                                    else{
                                        console.log("this.agents doesn't contain sprite with id " + (removeMe.gameObject.body.id));
                                        console.log(this.agents);
                                    }
                                }
                            }
                        }


                        if ((bodyA.gameObject.texture.key == 'star' && bodyB.gameObject.texture.key == 'player') || (bodyB.gameObject.texture.key == 'star' && bodyA.gameObject.texture.key == 'player')) {
                            if (bodyA.gameObject != this.playerController.matterSprite && bodyB.gameObject != this.playerController.matterSprite) {
                                let x, y, brain;
                                if (bodyA.gameObject.texture.key == 'star') {
                                    x = bodyA.gameObject.x;
                                    y = bodyA.gameObject.y;
                                    brain = bodyB.gameObject.parentdinges.brain;
                                    bodyA.gameObject.destroy();
                                }
                                else {
                                    x = bodyB.gameObject.x;
                                    y = bodyB.gameObject.y;
                                    brain = bodyA.gameObject.parentdinges.brain;
                                    bodyB.gameObject.destroy();
                                }
                                let newdude = newAgent(this.matter.add.sprite(0, 0, 'player', 4), brain.createMutation()); 
                                this.agents.push(newdude);

                                const body = M.Bodies.rectangle(sx, sy, w * 0.75, h, { chamfer: { radius: 10 } });
                                newdude.sensors.bottom = M.Bodies.rectangle(sx, h, sx, 5, { isSensor: true });
                                newdude.sensors.left = M.Bodies.rectangle(sx - w * 0.45, sy, 5, h * 0.25, { isSensor: true });
                                newdude.sensors.right = M.Bodies.rectangle(sx + w * 0.45, sy, 5, h * 0.25, { isSensor: true });
                                
                                const compoundBody = M.Body.create({
                                    parts: [
                                        body,
                                        newdude.sensors.bottom,
                                        newdude.sensors.left,
                                        newdude.sensors.right
                                    ],
                                    friction: 0.01,
                                    restitution: 0.05 // Prevent body from sticking against a wall
                                });
                                
                                newdude.matterSprite
                                    .setExistingBody(compoundBody)
                                    .setFixedRotation() // Sets max inertia to prevent rotation
                                    .setPosition(x, y);

                                newdude.matterSprite.setVelocityX(5);  // glitchy?
                                newdude.matterSprite.setVelocityY(-5);
                            }
                        }
                    }

                    if (bodyA === playerBody || bodyB === playerBody)
                    {
                        continue;
                    }
                    else if (bodyA === bottom || bodyB === bottom)
                    {
                        // Standing on any surface counts (e.g. jumping off of a non-static crate).
                        agentsandplayer[j].numTouching.bottom += 1;
                    }
                    else if ((bodyA === left && bodyB.isStatic) || (bodyB === left && bodyA.isStatic))
                    {
                        // Only static objects count since we don't want to be blocked by an object that we
                        // can push around.
                        agentsandplayer[j].numTouching.left += 1;
                    }
                    else if ((bodyA === right && bodyB.isStatic) || (bodyB === right && bodyA.isStatic))
                    {
                        agentsandplayer[j].numTouching.right += 1;
                    }
                }
            }
        }, this);

        // Update over, so now we can determine if any direction is blocked
        this.matter.world.on('afterupdate', function (event) {
            let agentsandplayer = this.agents.concat(this.playerController);
            for (let i in agentsandplayer) {
                agentsandplayer[i].blocked.right = (agentsandplayer[i].numTouching.right > 0 ? true : false);
                agentsandplayer[i].blocked.left = (agentsandplayer[i].numTouching.left > 0 ? true : false);
                agentsandplayer[i].blocked.bottom = (agentsandplayer[i].numTouching.bottom > 0 ? true : false);
            }
        }, this);

        this.input.on('pointerdown', function () {
            this.matter.world.drawDebug = !this.matter.world.drawDebug;
            this.matter.world.debugGraphic.visible = this.matter.world.drawDebug;
        }, this);
    }

    update (time, delta)
    {
        let matterSprite = this.playerController.matterSprite;

        if (this.cursors.left.isDown && !this.playerController.blocked.left)
        {
            this.playerController.smoothedControl.moveLeft(delta);
            matterSprite.anims.play('left', true);

            // Lerp the velocity towards the max run using the smoothed controls. This simulates a
            // player controlled acceleration.
            let oldVelocityX = matterSprite.body.velocity.x;
            let targetVelocityX = -this.playerController.speed.run;
            let newVelocityX = Phaser.Math.Linear(oldVelocityX, targetVelocityX, -this.playerController.smoothedControl.value);

            matterSprite.setVelocityX(newVelocityX);
        }
        else if (this.cursors.right.isDown && !this.playerController.blocked.right)
        {
            this.playerController.smoothedControl.moveRight(delta);
            matterSprite.anims.play('right', true);

            // Lerp the velocity towards the max run using the smoothed controls. This simulates a
            // player controlled acceleration.
            let oldVelocityX = matterSprite.body.velocity.x;
            let targetVelocityX = this.playerController.speed.run;
            let newVelocityX = Phaser.Math.Linear(oldVelocityX, targetVelocityX, this.playerController.smoothedControl.value);

            matterSprite.setVelocityX(newVelocityX);
        }
        else
        {
            this.playerController.smoothedControl.reset();
            matterSprite.anims.play('idle', true);
        }

        // Jumping & wall jumping

        // Add a slight delay between jumps since the sensors will still collide for a few frames after
        // a jump is initiated
        const canJump = (time - this.playerController.lastJumpedAt) > 250;
        if (this.cursors.up.isDown & canJump)
        {
            if (this.playerController.blocked.bottom)
            {
                matterSprite.setVelocityY(-this.playerController.speed.jump);
                this.playerController.lastJumpedAt = time;
            }
            else if (this.playerController.blocked.left)
            {
                // Jump up and away from the wall
                matterSprite.setVelocityY(-this.playerController.speed.jump);
                matterSprite.setVelocityX(this.playerController.speed.run);
                this.playerController.lastJumpedAt = time;
            }
            else if (this.playerController.blocked.right)
            {
                // Jump up and away from the wall
                matterSprite.setVelocityY(-this.playerController.speed.jump);
                matterSprite.setVelocityX(-this.playerController.speed.run);
                this.playerController.lastJumpedAt = time;
            }
        }

        this.smoothMoveCameraTowards(matterSprite, 0.9);

        if (this.cursors.down.isDown) {
            if ( ! this.downWasPressed) {
                let starspr = this.matter.add.sprite(this.playerController.matterSprite.x, this.playerController.matterSprite.y, 'star');
                if (this.cursors.right.isDown) {
                    starspr.x += 20;
                }
                else if (this.cursors.left.isDown) {
                    starspr.x -= 20;
                }
                else {
                    starspr.y -= 90;
                }
                this.downWasPressed = true;
            }
        }
        else {
            this.downWasPressed = false;
        }

        for (let i = 0; i < this.agents.length; i++) {
            matterSprite = this.agents[i].matterSprite;
            let relativeplayerx = (this.playerController.matterSprite.x - matterSprite.x) / 200;
            if (relativeplayerx > 1) relativeplayerx = 1;
            if (relativeplayerx < -1) relativeplayerx = -1;

            let jumpspeed = matterSprite.body.velocity.y / this.agents[i].speed.jump;
            if (jumpspeed > 1) jumpspeed = 1;
            if (jumpspeed < -1) jumpspeed = -1;

            // let sensors = [relativeplayerx, 0, 0, 0, 0];
            let sensors = [
                    relativeplayerx,
                    Math.max(matterSprite.body.velocity.x / this.agents[i].speed.run, 1),
                    jumpspeed,
                ];

            let outputs = this.agents[i].brain.decide(sensors);

            let decisions = {
                left: outputs[0],
                right: outputs[1],
                jump: outputs[2],
            };

            if (decisions.left && !this.agents[i].blocked.left)
            {
                this.agents[i].smoothedControl.moveLeft(delta);
                matterSprite.anims.play('left', true);

                // Lerp the velocity towards the max run using the smoothed controls. This simulates a
                // player controlled acceleration.
                let oldVelocityX = matterSprite.body.velocity.x;
                let targetVelocityX = -this.agents[i].speed.run;
                let newVelocityX = Phaser.Math.Linear(oldVelocityX, targetVelocityX, -this.agents[i].smoothedControl.value);

                matterSprite.setVelocityX(newVelocityX);
            }
            else if (decisions.right && !this.agents[i].blocked.right)
            {
                this.agents[i].smoothedControl.moveRight(delta);
                matterSprite.anims.play('right', true);

                // Lerp the velocity towards the max run using the smoothed controls. This simulates a
                // player controlled acceleration.
                let oldVelocityX = matterSprite.body.velocity.x;
                let targetVelocityX = this.agents[i].speed.run;
                let newVelocityX = Phaser.Math.Linear(oldVelocityX, targetVelocityX, this.agents[i].smoothedControl.value);

                matterSprite.setVelocityX(newVelocityX);
            }
            else
            {
                this.agents[i].smoothedControl.reset();
                matterSprite.anims.play('idle', true);
            }

            // Jumping & wall jumping

            // Add a slight delay between jumps since the sensors will still collide for a few frames after a jump is initiated
            const canJump = (time - this.agents[i].lastJumpedAt) > 250;
            if (decisions.jump & canJump)
            {
                if (this.agents[i].blocked.bottom)
                {
                    matterSprite.setVelocityY(-this.agents[i].speed.jump);
                    this.agents[i].lastJumpedAt = time;
                }
                else if (this.agents[i].blocked.left)
                {
                    // Jump up and away from the wall
                    matterSprite.setVelocityY(-this.agents[i].speed.jump);
                    matterSprite.setVelocityX(this.agents[i].speed.run);
                    this.agents[i].lastJumpedAt = time;
                }
                else if (this.agents[i].blocked.right)
                {
                    // Jump up and away from the wall
                    matterSprite.setVelocityY(-this.agents[i].speed.jump);
                    matterSprite.setVelocityX(-this.agents[i].speed.run);
                    this.agents[i].lastJumpedAt = time;
                }
            }
        }
    }

    smoothMoveCameraTowards (target, smoothFactor)
    {
        if (smoothFactor === undefined) { smoothFactor = 0; }
        this.cam.scrollX = smoothFactor * this.cam.scrollX + (1 - smoothFactor) * (target.x - this.cam.width * 0.5);
        this.cam.scrollY = smoothFactor * this.cam.scrollY + (1 - smoothFactor) * (target.y - this.cam.height * 0.5);
    }
}

const config = {
    type: Phaser.AUTO,
    width: 1400,
    height: 800,
    backgroundColor: '#000000',
    parent: 'gamediv',
    physics: {
        default: 'matter',
        matter: {
            gravity: { y: 1 },
            enableSleep: false,
        }
    },
    scene: Example
};

const game = new Phaser.Game(config);

